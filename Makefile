# DotEmacs/Makefile
# 2011-8-30

PREFIX=~

empty:

debug:
	ln -sfv $(CURDIR)/src/dot-emacs $(PREFIX)/.emacs
	mkdir -p $(PREFIX)/.emacs.d
	for f in `ls $(CURDIR)/src/*.embundle`; do ln -sfv $$f $(PREFIX)/.emacs.d; done
