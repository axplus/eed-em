;; cmd-shift-`
(global-set-key [8388734] 'switch-to-previous-frame)
;; cmd-shift-escape, for kbc
(global-set-key (quote [S-s-escape]) (quote switch-to-previous-frame))
