(add-to-list 'mmm-mode-ext-classes-alist '(c++-mode "\\.mm$" mmm-objc-mode))
(add-to-list 'mmm-mode-ext-classes-alist '(c-mode "\\.m$" mmm-objc-mode))
(add-to-list 'mmm-mode-ext-classes-alist '(c-mode "\\.h$" mmm-objc-mode))
