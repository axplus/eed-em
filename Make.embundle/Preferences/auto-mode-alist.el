(add-to-list 'auto-mode-alist '("(M|m)akefile$" . makefile-mode))
(add-to-list 'auto-mode-alist '("Makefile.symbol$" . makefile-mode))
(add-to-list 'auto-mode-alist '("Makefile.template$" . makefile-mode))
(add-to-list 'auto-mode-alist '("\\.mak$" . makefile-mode))
