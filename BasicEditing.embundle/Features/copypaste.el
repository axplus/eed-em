;; copypaste.el
;; 2011-8-9
;;
;; 改进Emacs默认的复制粘贴功能，不要再把删除的东西放到kill-ring里面去了，
;; 郁闷死我了


(provide 'copypaste)


(defun kill-region-without-kill-ring (beg end &optional yank-handler)
  "查看了simple.el的实现，发现其实只要重载掉kill-region就可以实现
控制kill-ring的目的了，不用重载每一个操作"
  (interactive (list (point) (mark)))
  (unless (and beg end)
    (error "The mark is not set now, so there is no region"))
  (delete-region beg end))


(defun kill-region-to-pasteboard-1 (beg end &optional yank-handler)
  (interactive (list (region-beginning)
                     (region-end)))
  (when (use-region-p)
    (kill-region-primitive beg end)))


;; 在没有选择区域的情况下按cmd-c没有任何反应


(defun ns-copy-including-secondary-1 ()
  (interactive)
  (when (use-region-p)
    (ns-copy-including-secondary-primitive)))


(fset 'kill-region-primitive (symbol-function 'kill-region))


(fset 'kill-region 'kill-region-without-kill-ring)


(fset 'ns-copy-including-secondary-primitive
     (symbol-function 'ns-copy-including-secondary))


(fset 'ns-copy-including-secondary 'ns-copy-including-secondary-1)


;; hotkeys for copy/paste


;; cmd-x
(global-set-key [8388728] (quote kill-region-to-pasteboard-1))


;; ctrl-w
(global-unset-key [?\C-w]) ;; (quote kill-region-to-pasteboard-1))
